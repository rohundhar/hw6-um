#include <stdio.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>


/*
 * Contributors: Cornelia Wilson and Rohun Dhar 
 * Date: 4/12/2016
 */


int main(int argc, char **argv)
{
    (void)argc;
    FILE *input = NULL;
    struct stat name; 
    input = fopen(argv[1], "r");
    (void)input;
    stat(argv[1], &name);

    fprintf(stderr, "%d\n", (int)name.st_size);

    return 0;
}