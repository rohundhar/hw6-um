#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "run_UM.h"
#include "UM_struct.h"


/*
 * Contributors: Cornelia Wilson and Rohun Dhar 
 * Date: 4/12/2016
 */
static inline instruction parse_instruction(UM_T program, instruction inst);

static inline void execute_instruction(UM_T program, instruction inst);

static inline void conditional_move (UM_T program, instruction inst);
static inline void seg_load (UM_T program, instruction inst);
static inline void seg_store (UM_T program, instruction inst);
static inline void add (UM_T program, instruction inst);
static inline void mult (UM_T program, instruction inst);
static inline void divide (UM_T program, instruction inst);
static inline void bit_nand (UM_T program, instruction inst);
static inline void halt (UM_T program, instruction inst);
static inline void map_seg (UM_T program, instruction inst);
static inline void unmap_seg (UM_T program, instruction inst);
static inline void output (UM_T program, instruction inst);
static inline void input (UM_T program, instruction inst);
static inline void load_p (UM_T program, instruction inst);
static inline void load_val (UM_T program, instruction inst);
static inline void free_prgm(UM_T program, instruction inst);
//int count = 0;

void run(UM_T program)
{
        /* inst will be reused each interation of the program instead of being
        repeatedly re-allocated in order to save time and space */
        instruction inst;
        inst = malloc(sizeof(*inst));
        while (program->program_ctr != program->program_len){
                inst = parse_instruction(program, inst);

                execute_instruction(program, inst);
               //count++;
        }
        //fprintf(stderr, "instructions: %d\n", count);

        free_prgm(program, inst);    

        exit(0);
}

static inline instruction parse_instruction(UM_T program, instruction inst)
{
       
        uint32_t word =
                 get_word(program->segment_handler, 0, program->program_ctr);
        uint32_t mask = ~(uint32_t)0;
        mask = (mask << 28);
        inst->code = (mask & word) >> 28;

        /*the code LV has a slightly different configuration than the other
        codes */
        if (inst->code == LV){
                mask = ~0;
                mask = (mask << 29 >> 4);
                inst->a = (mask & word) >> 25;


                mask = ~0;
                mask = mask >> 7;
                inst->val = (mask & word);

        }
        /*sets the a, b, c registers and instruction code into the struct */
        else{
                mask = ~0;
                mask = (mask << 29 >> 29);

                inst->c = mask & word;
                inst->b = ((mask << 3) & word) >> 3;
                inst->a = ((mask << 6) & word) >> 6;
        }
       

        return inst;
}

static inline void execute_instruction(UM_T program, instruction inst)
{
        switch(inst->code) {
                case CMOV:
                        conditional_move(program, inst);
                        return;
                case SLOAD:
                        seg_load(program, inst);
                        return;
                case SSTORE:
                        seg_store(program, inst);
                        return;
                case ADD:
                        add(program, inst);
                        return;
                case MUL:
                        mult(program, inst);
                        return;
                case DIV:
                        divide(program, inst);
                        return;
                case NAND:
                        bit_nand(program, inst);
                        return;
                case HALT:
                        halt(program, inst);
                        return;
                case ACTIVATE:
                        map_seg(program, inst);
                        return;
                case INACTIVATE:
                        unmap_seg(program, inst);
                        return;
                case OUT:
                        output(program, inst);
                        return;
                case IN:
                        input(program, inst);
                        return;
                case LOADP:
                        load_p(program, inst);
                        return;
                case LV:
                        load_val(program, inst);
                        return;
                default:
                        fprintf(stderr, "Invalid instruction\n");
                        free_prgm(program, inst);
                        exit(EXIT_FAILURE);
                        return;
        }
}

static inline void conditional_move (UM_T program, instruction inst)
{
        if (program->registers[inst->c] != 0){
                program->registers[inst->a] = program->registers[inst->b]; 
        }
        program->program_ctr++;
        return;
}

static inline void seg_load (UM_T program, instruction inst)
{
    program->registers[inst->a] = get_word( program->segment_handler, 
                                            program->registers[inst->b],
                                            program->registers[inst->c]);
    program->program_ctr++;

}

static inline void seg_store (UM_T program, instruction inst)
{
        set_word(program->segment_handler,  program->registers[inst->a],
                                            program->registers[inst->b], 
                                            program->registers[inst->c]);
        program->program_ctr++;


}
static inline void add (UM_T program, instruction inst)
{
        program->registers[inst->a] = ( program->registers[inst->b] +
                                        program->registers[inst->c]);
        program->program_ctr++;

}

static inline void mult (UM_T program, instruction inst)
{
        program->registers[inst->a] = ( program->registers[inst->b] * 
                                        program->registers[inst->c]);
        program->program_ctr++;

}

static inline void divide (UM_T program, instruction inst)
{
     if (program->registers[inst->c] == 0){
        fprintf(stderr, "Cannot divide by 0\n");
        free_prgm(program, inst);
        exit(1);
     }
        program->registers[inst->a] = (program->registers[inst->b] / 
                                       program->registers[inst->c]);
        program->program_ctr++;

}

static inline void bit_nand (UM_T program, instruction inst)
{
        program->registers[inst->a] = ~(program->registers[inst->b] &
                                        program->registers[inst->c]);
        program->program_ctr++;

}

static inline void halt (UM_T program, instruction inst)
{
       // fprintf(stderr, "instructions: %d\n", count);

        free_prgm(program, inst);    
        exit(0);

}

static inline void map_seg (UM_T program, instruction inst)
{
        int index = new_segment(program->segment_handler,
                                program->registers[inst->c]);
        program->registers[inst->b] = index;
        program->program_ctr++;

}

static inline void unmap_seg (UM_T program, instruction inst)
{
        if (program->registers[inst->c] == 0){
            free_prgm(program, inst);
            exit(1);
        }
        free_segment(program->segment_handler, program->registers[inst->c]);
        program->program_ctr++;

}

static inline void output (UM_T program, instruction inst)
{
        if (program->registers[inst->c] > 255){
               free_prgm(program, inst); 
               exit(1);   

        }
        else{
                fputc(program->registers[inst->c], stdout);
                program->program_ctr++;

        }
}

static inline void input (UM_T program, instruction inst)
{
        uint32_t input = fgetc(stdin);
        if (input > 255){
               free_prgm(program, inst);
               exit(1);   
        }
        program->registers[inst->c] = input;
        program->program_ctr++;

}

static inline void load_p (UM_T program, instruction inst)
{
       if (program->registers[inst->b] != 0){
            duplicate_segment(  program->segment_handler, 
                                program->registers[inst->b]);
            program->program_len = segment_length(program->segment_handler, 0);
       }
       program->program_ctr = program->registers[inst->c];
}

static inline void load_val (UM_T program, instruction inst)
{
        program->registers[inst->a] = inst->val;
        program->program_ctr++;
        
}
static inline void free_prgm(UM_T program, instruction inst)
{
                Segment_free(program->segment_handler);
                free(inst);
                fclose(program->program_src);

                free(program);
}