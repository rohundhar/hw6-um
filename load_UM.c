#include <stdint.h>
#include <stdio.h>
#include "load_UM.h"
#include "UM_struct.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

/*
 * Contributors: Cornelia Wilson and Rohun Dhar 
 * Date: 4/12/2016
 */
#define WORD_SIZE 4
UM_T load(FILE *input, char* filename)
{
        UM_T T;
        T = malloc(sizeof(*T));
        T->program_ctr = 0;
        T->segment_handler = setup_segment();


        struct stat info; 
        if (stat(filename, &info) != 0){
            fprintf(stderr, "File corrupt\n");
            exit(1);
        }
        /* the struct "info" contains information about the size of the 
        input file, including byte size */
        int program_size = (int)info.st_size / WORD_SIZE;

        T->program_len = program_size;
        T->program_src = input;
        
        /* initializes all register values to 0 to avoid valgrind errors */
        for (int i = 0; i < 8; i++)
        {
            T->registers[i] = 0;
        }

        new_segment(T->segment_handler, program_size);

        for (int i = 0; i < program_size; i++)
        {
                uint32_t word = 0;
                uint32_t mask = 0;

                /*store the word one byte at a time */
                for (int j = 3; j >= 0; j--)
                {
                    uint32_t c = fgetc(input);
                    mask = (mask | (c << (8 * j)));
                }
                word = mask;

                set_word(T->segment_handler, 0, i,  word); 

        }

           
        return T;


}