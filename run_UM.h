#ifndef RUN_UM_INCLUDED
#define RUN_UM_INCLUDED
#define UM_T UM_T

#include "UM_struct.h"

/*
 * Contributors: Cornelia Wilson and Rohun Dhar 
 * Date: 4/12/2016
 */
 
/*
 *This function will take in a UM_T struct, and run through the segment mapped
 * at index 0, running each instruction consecutively until it reaches the
 * end of the program. There are only 14 recognized instructions, and if the 
 * user acts irresponsibly this function will clean up all memory used and 
 * exit failure. If the program is halted by the user or reaches the end, it
 * exits with success.
 */
void run(UM_T program);

#undef UM_T
#endif 