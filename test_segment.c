#include "segments.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "stack.h"
/*
 * Contributors: Cornelia Wilson and Rohun Dhar 
 * Date: 4/12/2016
 */

/*****************************************************
*                                                    *
*                                                    *
*                                                    *
* This is a test file exclusively for the segment    *
* implementation. Both tests 4 and 5 intentionally   *
* reach outside of their boundaries.                 *
*                                                    *
*                                                    *
*                                                    *
******************************************************/
void test1();
void test2();
void test3();
void test4();
void test5();
void test6();


int test_counter = 0;

int main()
{

    test1(); 
    fprintf(stderr, "Passed test %d\n", test_counter);

    test2();
    fprintf(stderr, "Passed test %d\n", test_counter);

    test3();
    fprintf(stderr, "Passed test %d\n", test_counter); 

    test4();
    fprintf(stderr, "Passed test %d\n", test_counter);

    test5();
    fprintf(stderr, "Passed test %d\n", test_counter);

    test6();
    fprintf(stderr, "Passed test %d\n", test_counter);




}

/* creates 3 segments (in indices 0, 1, 2), then frees the segments located at
0 and 2. So when a new segment is created, the program will use the 0th index 
again. If it does not use it to create the new segment, the test fails */
void test1()
{
    Segment_T segment = setup_segment();

    new_segment(segment, 10);

    new_segment(segment, 5);

    new_segment(segment, 2);

    free_segment(segment, 2);
    free_segment(segment, 0);
    if (new_segment(segment, 8) != 0){
        fprintf(stderr, "test 1 failed\n");
    }

    set_word(segment, 1,  3, 10000);

    Segment_free(segment);
    test_counter++;

}
/* input 10000 new segments into the segment_handler and then retrieve each of 
the values. If they are not the same, the test fails */ 
void test2()
{
    Segment_T segment = setup_segment();

    for (int i = 1; i < 10000; i++)
    {
        new_segment(segment, i);
        set_word(segment, i-1, i-1, i + 10);
    }   
    for (int i = 1; i < 10000; i++)
    {
        if (get_word(segment, i-1, i-1) != (uint32_t)i+10){
            fprintf(stderr, "test 2 failed\n");
            exit(EXIT_FAILURE);
        }
        free_segment(segment, i-1);
    }
    Segment_free(segment);
    test_counter++;
    return;
}


/* loop 10000: create a segment, and then free it to check if the program will
re-use the same segment's memory location. If the index of the segment is 
greater than 1, then it fails */ 
void test3()
{

    Segment_T segment = setup_segment();
    for (int i = 0; i < 10000; i++)
    {
        if (new_segment(segment, 10) > 1){
            fprintf(stderr, "test 3 failed\n");
        }
        free_segment(segment, 0);
    }

    Segment_free(segment);
    test_counter++;
    return;
}

/* attempt to access the word of a segment that hasn't neen setup */ 
void test4()
{
    Segment_T segment = setup_segment();

    get_word(segment, 5, 5);


    Segment_free(segment);

    test_counter++;
    return;
}

/* create a segment and overreach the boundaries of that segment */ 
void test5()
{
    Segment_T segment = setup_segment();

    new_segment(segment, 5);
    get_word(segment, 0, 7);

    Segment_free(segment);
    test_counter++;
    return;


}
void test6()
{
    Segment_T segment = setup_segment();

    for (int i = 0; i < 10; i++)
    {
        new_segment(segment, 10);
    }
    for (int j = 0; j < 10; j++){
        set_word(segment, 5, j, j);
    }
    duplicate_segment(segment, 5);
    for (int i = 0; i < 10; i++){
        fprintf(stderr, "%d\n", get_word(segment, 0, i));
    }
    Segment_free(segment);
    test_counter++;


}

