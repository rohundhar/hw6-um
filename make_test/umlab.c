/*
 * umlab.c
 * 
 * The functions defined in this lab should be linked against umlabwrite.c
 * to produce a unit test writing program. Any additional functions and unit
 * tests written for the lab go here.
 */

#include <stdint.h>
#include <stdio.h>

#include <assert.h>
#include <seq.h>


typedef uint32_t Um_instruction;
typedef enum Um_opcode {
        CMOV = 0, SLOAD, SSTORE, ADD, MUL, DIV,
        NAND, HALT, ACTIVATE, INACTIVATE, OUT, IN, LOADP, LV
} Um_opcode;
typedef enum Um_register { r0 = 0, r1, r2, r3, r4, r5, r6, r7 } Um_register;


/* Functions that return the two instruction types */

Um_instruction three_register(Um_opcode op, int ra, int rb, int rc);
Um_instruction loadval(unsigned ra, unsigned val);

Um_instruction condmov(Um_register a, Um_register b, Um_register c)
{
    return three_register(CMOV, a, b, c);
}

Um_instruction seg_load(Um_register a, Um_register b, Um_register c)
{
    return three_register(SLOAD, a, b, c);

}
Um_instruction seg_store(Um_register a, Um_register b, Um_register c)
{
    return three_register(SSTORE, a, b, c);
 
}
Um_instruction mult(Um_register a, Um_register b, Um_register c)
{
    return three_register(MUL, a, b, c);
 
}
Um_instruction div(Um_register a, Um_register b, Um_register c)
{
    return three_register(DIV, a, b, c);
 
}
Um_instruction bitnand(Um_register a, Um_register b, Um_register c)
{
    return three_register(NAND, a, b, c);
 
}
Um_instruction map_seg(Um_register a, Um_register b, Um_register c){
    return three_register(ACTIVATE, a, b, c);
 
}
Um_instruction unmap_seg(Um_register a, Um_register b, Um_register c){
    return three_register(INACTIVATE, a, b, c);
 
}
Um_instruction input(Um_register a, Um_register b, Um_register c){
    return three_register(IN, a, b, c);
 
}
Um_instruction loadp(Um_register a, Um_register b, Um_register c){
    return three_register(LOADP, a, b, c);
 
}



Um_instruction loadval(unsigned ra, unsigned val)
{
        Um_instruction load = 0;
        load = (load | val);
        load = (load | (LV << 28));
        load = (load | (ra << 25));
        return load;
}



Um_instruction three_register(Um_opcode op, int ra, int rb, int rc)
{
        Um_instruction instruction = 0;
        instruction = (instruction | (op << 28));

        instruction = (instruction | (ra << 6));
        instruction = (instruction | (rb << 3));
        instruction = (instruction | rc);


        return instruction;

}
/* Wrapper functions for each of the instructions */

static inline Um_instruction halt(void) 
{

        return three_register(HALT, 0, 0, 0);

}


static inline Um_instruction add(Um_register a, Um_register b, Um_register c) 
{
        return three_register(ADD, a, b, c);
}

Um_instruction output(Um_register c)
{
        Um_instruction output = 0;

        output = (output | (OUT << 28));
        output = output | c;

        return output;
}



/* Functions for working with streams */

static inline void emit(Seq_T stream, Um_instruction inst)
{
        assert(sizeof(inst) <= sizeof(uintptr_t));
        Seq_addhi(stream, (void *)(uintptr_t)inst);
}

extern void Um_write_sequence(FILE *output, Seq_T stream){
        for (int i = 0; i < Seq_length(stream); i++){
                Um_instruction inst = ((uintptr_t)Seq_get(stream, i));
                for (int j = 3; j >= 0; j--){
                        uint32_t mask = 255;
                        mask = (inst & (mask << (8 * j)));
                        mask = mask >> (8 * j);
                        putc(mask, output);
                        //putc("\n", output);
                }
               // fprintf(output, "\n");


        }
}


/* Unit tests for the UM */

void emit_halt_test(Seq_T stream)
{
        emit(stream, halt());

}

void emit_verbose_halt_test(Seq_T stream)
{

        emit(stream, halt());
        emit(stream, loadval(r1, 'B'));
        emit(stream, output(r1));
        emit(stream, loadval(r1, 'a'));
        emit(stream, output(r1));
        emit(stream, loadval(r1, 'd'));
        emit(stream, output(r1));
        emit(stream, loadval(r1, '!'));
        emit(stream, output(r1));
        emit(stream, loadval(r1, '\n'));
        emit(stream, output(r1)); 
}
void add_test(Seq_T stream)
{
        emit(stream, add(r1, r2, r3));
        emit(stream, halt());
}
void print_six(Seq_T stream)
{
        emit(stream, loadval(r1, 48));
        emit(stream, loadval(r2,6));
        emit(stream, add(r3, r1, r2));
        emit(stream, output(r3));
        emit(stream, halt());

}
void map_segment(Seq_T stream)
{
    emit(stream, loadval(r3, 65));
    for (int i = 0; i < 100000; ++i)
    {
        emit(stream, map_seg(r1, r2, r3));
        emit(stream, unmap_seg(r1, r1, r2));


    }
    emit(stream, map_seg(r1, r2, r3));
    emit(stream, loadval(r4, 23));
    emit(stream, seg_store(r2, r4, r3));
    emit(stream, seg_load(r5, r2, r4));
    emit(stream, output(r5));
    emit(stream, halt());
}
void cond_test(Seq_T stream)
{
    emit(stream,loadval(r1, 42));
    emit(stream, loadval(r2, 38));
    emit(stream, condmov(r3, r2, r1));
    emit(stream, output(r3));
    emit(stream, loadval(r1, 0));
    emit(stream, condmov(r3, r1, r1));
    emit(stream, output(r3));
    emit(stream, halt());

}
void nand_test(Seq_T stream)
{
    emit(stream, loadval(r1, 251));
    emit(stream, loadval(r2, 221));
    emit(stream, bitnand(r3, r1, r2));
    emit(stream, output(r3));
    emit(stream, halt());


}
void multdiv_test(Seq_T stream)
{
    emit(stream, loadval(r1, 84));
    emit(stream, loadval(r2, 2));
    emit(stream, div(r3, r1, r2));
    emit(stream, output(r3));
    emit(stream, loadval(r2, 3));
    emit(stream, mult(r4, r2, r3));
    emit(stream, output(r4));
    emit(stream, halt());

}

/* creates 50 million segments of size 10, and this code 
is to test, according to the specifications, its performance
for 50 million instruction */ 
void one_sec_test(Seq_T stream)
{
     emit((stream), loadval(r3, 10));
    for (int i = 1; i < 50000000; ++i)
    {
        emit((stream), map_seg(r1, r2, r3));
    }
}
void input_test(Seq_T stream)
{
    emit(stream, input(r1, r2, r3));
    emit(stream, loadval(r2, 5));
    emit(stream, add(r1, r2, r3));
    emit(stream, output(r1));
    emit(stream, halt());
}