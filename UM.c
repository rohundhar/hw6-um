#include "run_UM.h"
#include "load_UM.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

/*
 * Contributors: Cornelia Wilson and Rohun Dhar 
 * Date: 4/12/2016
 */

int main(int argc, char **argv)
{
        (void) argc;
        FILE* input = NULL;
        input = fopen(argv[1], "r");
        if (input == NULL){
                fprintf(stderr, "file is null\n" );
                exit(1);
        }


        UM_T program = load(input, argv[1]);

        run(program);
        
        return 0;
}