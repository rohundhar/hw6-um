#ifndef LOAD_UM_INCLUDED
#define LOAD_UM_INCLUDED
#define UM_T UM_T

#include <stdio.h>
#include "UM_struct.h"
/*
 * Contributors: Cornelia Wilson and Rohun Dhar 
 * Date: 4/12/2016
 */
 
/*
 *This function will read in input from a specified file, reading
 * in words one byte at a time. It will store the entire program
 * inside of index 0 of a segment_handler and return a UM_T struct, which 
 * contains all information about the program/
 */
UM_T load(FILE *input, char* filename);

#undef UM_T
#endif 