#ifndef UM_STRUCT_INCLUDED
#define UM_STRUCT_INCLUDED

#include "segments.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

/*
 * Contributors: Cornelia Wilson and Rohun Dhar 
 * Date: 4/12/2016
 */

/* This enum defines all valid instruction codes for the Universal Machine */
typedef enum Um_opcode {
        CMOV = 0, SLOAD, SSTORE, ADD, MUL, DIV,
        NAND, HALT, ACTIVATE, INACTIVATE, OUT, IN, LOADP, LV
} Um_opcode;

/*
 *The UM_T represents the entire program. It contains the segment handler,
 *the registers, program counter, length, and file source. This will be passed
 *around by both load_UM and run_UM in order to communicate with each other
 *efficiently. 
 */
typedef struct UM_T {
        Segment_T segment_handler;
        uint32_t registers[8];
        int program_ctr;
        int program_len;

        FILE* program_src;
} *UM_T;

/*
 *This struct will represent a single UM instruction, which will then be 
 *executed. The code is as defined above, one of fourteen possibilities. The 
 *registers a, b, c are defined here as well, and because they are confined to
 *a 3-bit value, we have chosen to represent them as uint8_t types to save 
 *space;
 */
typedef struct instruction {
        Um_opcode code;
        uint32_t val; /* for load val instruction */

        uint8_t a, b, c;
} *instruction;

#endif
