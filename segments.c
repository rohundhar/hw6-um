#include "segments.h"
#include <stdlib.h>
#include <stdio.h>
#include "stack.h"
#include "seq.h"
#include "uarray.h"
#define HINT 100
/*
 * Contributors: Cornelia Wilson and Rohun Dhar 
 * Date: 4/12/2016
 */
 
struct Segment_T {
        Stack_T stack;
        Seq_T array; 
};

typedef struct Array_T {
    uint32_t *array;
    int length;
} *Array_T;

static inline Array_T copy_array(Array_T old_array);
static inline Array_T create_array(int size);


Segment_T setup_segment()
{
        Segment_T T = malloc(sizeof(*T));
        if (T == NULL){
        exit(EXIT_FAILURE);
        }

        /* create new empty sequence and stack */
        T->array = Seq_new(HINT); 
        T->stack = Stack_new();
        return T;

}
int segment_length(Segment_T segments, int column)
{

        Array_T segment = Seq_get(segments->array, column);
        return segment->length;
}

void duplicate_segment(Segment_T segments, int column)
{
        if (column < 0 || column >= Seq_length(segments->array)){
            fprintf(stderr, "Out of bounds reach\n");
            Segment_free(segments);
            exit(1);
        }

        Array_T segment = Seq_get(segments->array, column);
        Array_T new_segment = copy_array(segment);

        free_segment(segments, 0);
        /*free_segment() will add the index 0 to the stack, but it is actually
        being used so we override this invariant of the function in this case*/
        int *ptr = Stack_pop(segments->stack);
        free(ptr);
        
        Seq_put(segments->array, 0, new_segment);
  
        return; 
}

uint32_t new_segment(Segment_T segments, int size) {

        if (size <= 0){
            Segment_free(segments);
            exit(1);
        }
        /* if the stack is empty, meaning that there are no available spaces 
        in the sequence, then add a new segment to the end */
        if (Stack_empty(segments->stack)){

            Array_T new_seg = create_array(size);
            Seq_addhi(segments->array, new_seg);
            return (Seq_length(segments->array) - 1);


        }
        /*if there is a value in the stack, then pop it and use that value 
        as the index of the new segment you create */
        else{

                int *ptr = Stack_pop(segments->stack);
                
                if (ptr == NULL){
                    Segment_free(segments);
                    exit(1);
                }
                
                uint32_t return_val = *ptr;
                free(ptr);

                Array_T new_seg = create_array(size);

                Seq_put(segments->array, return_val, new_seg);

                return return_val;
        }
}

uint32_t get_word(Segment_T segments, int column, int row){
        
        if (column < 0 || column >= Seq_length(segments->array)){
            fprintf(stderr, "Out of bounds\n");
            Segment_free(segments);
            exit(1);
            return 0;
        }
        Array_T segment = Seq_get(segments->array, column);
        if (segment == NULL){
            fprintf(stderr, "Out of bounds\n");
            Segment_free(segments);
            exit(1);
        }

        if (row < 0 || row >= segment->length){
            fprintf(stderr, "Out of bounds");
            Segment_free(segments);
            exit(1);
        }


        uint32_t value = segment->array[row];
        return value;
}

uint32_t set_word(Segment_T segments, int column, int row, uint32_t word){


        if (column < 0 || column >= Seq_length(segments->array)){
            fprintf(stderr, "Out of bounds\n");
            Segment_free(segments);
            exit(1);
        }
        Array_T segment = Seq_get(segments->array, column);

        if (segment == NULL){
            fprintf(stderr, "Out of bounds\n");
            Segment_free(segments);
            exit(1);
        }

        if (row < 0 || row >= segment->length){
            fprintf(stderr, "Out of bounds\n");
            Segment_free(segments);
            exit(1);
        }
        uint32_t old_val = segment->array[row];
        segment->array[row] = word;

        return old_val;
}
void free_segment(Segment_T segments, int column)
{
        if (column < 0 || column >= Seq_length(segments->array)){
            fprintf(stderr, "Out of bounds\n");
            Segment_free(segments);
            exit(1);
        }
        Array_T segment = Seq_get(segments->array, column);

        free(segment->array);
        free(segment);
 
        Seq_put(segments->array, column, NULL);

        /* This if statement protects us from seg faulting when performing 
        our deep cleansing using Segment_free(), at which point the stack 
        is already freed */ 
        if (segments->stack){
            int *ptr = malloc(sizeof(int));
            if (ptr == NULL){
                Segment_free(segments);
                exit(1);
            }
            *ptr = column;
            Stack_push(segments->stack, (void*)ptr); 
        }


}
void Segment_free(Segment_T segments){

        /* free all of the stack integer pointers */

        while (!Stack_empty(segments->stack)){
            int *ptr = Stack_pop(segments->stack);
            free(ptr);
        }
        Stack_free(&(segments->stack));

        /*free each individual segment before the entire sequence */ 
        for (int i = 0; i < Seq_length(segments->array);i++ ){

            Array_T T = Seq_get(segments->array, i);

            if (T != NULL){
                free_segment(segments, i);
            }
        }

        Seq_free(&(segments->array));

        free(segments);
}



static inline Array_T create_array(int size)
{
    Array_T new_array;
    new_array = malloc(sizeof(*new_array));
    new_array->array = malloc(sizeof(uint32_t) * size);
    for (int i = 0; i < size; i++)
    {
        new_array->array[i] = 0;
    }
    new_array->length = size;
    return new_array;
}
static inline Array_T copy_array(Array_T old_array)
{
    Array_T array_copy;

    array_copy = create_array(old_array->length);
    for (int i = 0; i < old_array->length; i++)
    {
        array_copy->array[i] = old_array->array[i];
    }
    return array_copy;
}