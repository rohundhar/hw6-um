#ifndef SEGMENTS_INCLUDED
#define SEGMENTS_INCLUDED
#define Segment_T Segment_T

#include <stdint.h>

typedef struct Segment_T *Segment_T;

/* initializes the segment_handler to be used */ 
Segment_T setup_segment();


/*
 *returns the length of the segment specific at column, if it is mapped
 */
int segment_length(Segment_T segments, int column);

/*
 * Makes a copy of the segment specified by column, and then places that newly
 * created copy in the 0th index of the segment_handler, effectively replacing
 * the 'program' itself. 
 * - It is a c.r.e to pass in a column that is less than 0 or greater than or
 * equal to the size of the segment_handler 
 */
void duplicate_segment(Segment_T segments, int column);

/* 
 * creates a new segment which has length of size and the function returns the 
 * index at which it stored the segments 
 * - It is a c.r.e. to pass in a size less than or equal to 0.
 */
uint32_t new_segment(Segment_T segments, int size); 

/*
 * gets the 32 bit word at the column and row requested and return it 
 * - it is a c.r.e. for the client to specify a column (our representation of 
 *   a pointer) that they have not previously allocated using new_segment.
 * - it is a c.r.e. for segments to be NULL 
 * - it is a c.r.e. for row to be greater than or equal to the size of the 
 *   segment
 */ 
uint32_t get_word(Segment_T segments, int column, int row);

/* 
 * Set a 32 bit word at the column and row requested. 
 * - it is a c.r.e. for the client to specify a column (our representation of  
 *   a pointer) that they have not previously allocated using new_segment().
 * - it is a c.r.e. for the client to specify a row greater than or equal to 
 *   the size of the segment.
 */
uint32_t set_word(Segment_T segments, int column, int row, uint32_t word); 

/*
 * Frees a segment that resides at the specified column, leaving it 
 * available to be re-allocated later 
 * - it is a c.r.e. for the client to specify a column (our representation of  
 *   a pointer) that they have not previously allocated using new_segment().
 */  
void free_segment (Segment_T segments, int column); 

/* 
 * performs a deep cleansing of the segment, deleting all possible segment 
 * values at every position 
 * - it is a c.r.e. for segments to be NULL 
 */ 
void Segment_free(Segment_T segments);

#undef Segment_T
#endif 